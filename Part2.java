package vgGrabbarna;

import java.awt.Color;
import robocode.HitByBulletEvent;
import robocode.HitWallEvent;
import robocode.Robot;
import robocode.ScannedRobotEvent;
import robocode.HitRobotEvent;


public class Part4 extends Robot {
    public Part4() {
    }

    public void run() {
        this.setBodyColor(Color.YELLOW);
        this.setGunColor(Color.YELLOW);
        this.setRadarColor(Color.black);
        this.setScanColor(Color.green);


        while(true) {
            this.ahead(100.0D);
            this.turnRight(20.0D);
            this.back(70.0D);
            this.turnGunLeft(40.0D);
            this.turnGunRight(180.0D);
        }
    }









    public void onScannedRobot(ScannedRobotEvent e) {

        this.setRadarColor(Color.MAGENTA);
		
        boolean friendOfMine;
        String target;
        target = e.getName();

        friendOfMine = target.contains("HitmanAgent");
        if (friendOfMine) {
            this.fire(1.0D);
            this.turnGunLeft(20.0D);
            this.fire(1.0D);

        }




        this.fire(3.0D);
        this.turnGunLeft(20.0D);
        this.fire(3.0D);
        this.back(70.0D);
        this.turnGunLeft(20.0D);





    }

    public void onHitByBullet(HitByBulletEvent e) {
        this.setRadarColor(Color.red);
        this.back(10.0D);
    }

    public void onHitWall(HitWallEvent e) {
        this.setRadarColor(Color.black);

        this.back(20.0D);
        this.turnLeft(30.0D);
        this.back(200.0D);
    }

    public void minimizeRadar(double enemy){

        double angle = getHeading() + enemy - getGunHeading();


        if(!(angle > -180 && angle <=180)){
            while(angle <= -180){
                angle += 360;
            }
            while(angle > 180){
                angle -= 360;
            }
        }
        turnGunRight(angle);
    }
}


    /* since this bot earns tons of survivalbonus

    wanted the bot to defend if my bot is stil alive after a long time,
     and save energy by not shotting.

     could be much more effective if OnMissedBullet e was triggered instead of onHitRobot,
     since gettime would be update more frequently


    public void onHitRobot(HitRobotEvent e) {

        if(e.getEnergy() <100 ){
            System.out.println("tid: "+getTime());
            if( e.getEnergy()<=18 ||getTime() >=600 && getTime() <1000   ){
                this.ahead(100.0D);
                this.turnRight(20.0D);
                this.back(70.0D);
                this.turnGunLeft(40.0D);
                this.turnGunRight(180.0D);

                this.ahead(100.0D);
                this.turnRight(20.0D);
                this.back(70.0D);
                this.turnGunLeft(40.0D);
                this.turnGunRight(180.0D);

                this.ahead(100.0D);
                this.turnRight(20.0D);
                this.back(70.0D);
                this.turnGunLeft(40.0D);
                this.turnGunRight(180.0D);

                this.ahead(100.0D);
                this.turnRight(20.0D);
                this.back(70.0D);
                this.turnGunLeft(40.0D);
                this.turnGunRight(180.0D);

            }

        }
    }

     */
